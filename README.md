## server-loaded-js-nashorn

Written as a proof of concept that the following technical requirement could be met: when a chart (e.g. a bar chart) needs to generated in the browser ***and*** server-side (for an emailed pdf report say), there ought to be a single piece of code which could accomplish this.

What needed to be avoided was the case where the configuration/options for the *same* chart (e.g. a line chart representing the energy usage of a specific device) was generated in 2 places using 2 distinct bits of code: JavaScript on the front-end and Java on the back-end. This would, one, make the code more difficult to maintain and, two, increase the likelihood of a chart in the browser looking different from that in the pdf report, generated on the server-side.    

The solution was to store the JavaScript code - responsible for generating the chart options - server-side. See [db-insert-options-code-block.sql](https://bitbucket.org/davidtcode/server-loaded-js-nashorn/src/b4c2b52d0a89c12096bbe8a9d807aba8f8b09d77/src/main/resources/db/db-insert-options-code-block.sql?at=master&fileviewer=file-view-default). When the UI loads, this JavaScript is loaded using jQuery's *getScript* function. For reports generated server-side, this same JavaScript code is executed using **Nashorn**, Java 8's JavaScript engine. See [ChartOptionsBuilderImpl](https://bitbucket.org/davidtcode/server-loaded-js-nashorn/src/b4c2b52d0a89c12096bbe8a9d807aba8f8b09d77/src/main/java/davidtcode/chart/impl/ChartOptionsBuilderImpl.java?at=master&fileviewer=file-view-default).

### The use of PhantomJS

PhantomJS, a headless browser, was used to actually render the chart and export it as a pdf. See [PhantomJSRasterizer](https://bitbucket.org/davidtcode/server-loaded-js-nashorn/src/b4c2b52d0a89c12096bbe8a9d807aba8f8b09d77/src/main/java/davidtcode/rasterizer/impl/PhantomJSRasterizer.java?at=master&fileviewer=file-view-default)

### Highcharts

*Highcharts* is the charting library that has been used. The script [highcharts-convert.js](https://bitbucket.org/davidtcode/server-loaded-js-nashorn/src/b4c2b52d0a89c12096bbe8a9d807aba8f8b09d77/src/main/resources/phantomjs/highcharts-convert.js?at=master), which is supplied by that library, is used to integrate with PhantomJS in order to render the charts and export them to pdfs.   

I'm using the non-commercial licence. See http://shop.highsoft.com/faq/non-commercial#what-is-non-commercial and http://creativecommons.org/licenses/by-nc/3.0/legalcode.

### Prerequisites and how to run it

You'll need:

* Java 8
* Maven 3+ 
* PhantomJS installed at C:\phantomjs-2.0.0-windows\bin\phantomjs.exe
    * Actually, you can install a different version in a different location but you'll need to edit the properties file at: [application.properties](https://bitbucket.org/davidtcode/server-loaded-js-nashorn/src/b4c2b52d0a89c12096bbe8a9d807aba8f8b09d77/src/main/resources/application.properties?at=master&fileviewer=file-view-default) 
* Run "mvn clean install" and deploy the built WAR file to a Servlet container.
    * Or run "mvn jetty:run" and access the application at http://localhost:8081/nashorn/
* From the UI, you can see the charts for the energy usage for 2 fictitious devices and generate pdf versions of those charts. The former generates the charts in the browser whilst the latter does so server-side. Both, **crucially**, use the same JavaScript code. 

### Some thoughts

* In a non-prototype environment, the Rasterizers servers (see [PhantomJSRasterizer](https://bitbucket.org/davidtcode/server-loaded-js-nashorn/src/b4c2b52d0a89c12096bbe8a9d807aba8f8b09d77/src/main/java/davidtcode/rasterizer/impl/PhantomJSRasterizer.java?at=master&fileviewer=file-view-default)) should be pooled. That is, more than 1 PhantomJS instance should be started in server mode.
* The Nashorn JavaScript engine is going to be different, in some ways, from browser engines such as WebKit or Gecko. The former, for example, had extended functionality which make sense within a Java context, such as an enhanced styled for loop. The "print" function is interpreted differently in both engines; the former has no console.log() and so on. As such, there ought to be some safe-guards in place: automated tests if the JavaScript just ships with the product or if they're something which can be added at runtime, a pre-validation step which ensures the code is good.
* The JavaScript code which is used to generate the configuration/options for the charting library is simply a function: it takes in the actual series of device readings and returns the information the library requires in order to create the chart. Those functions operate in the global namespace. A more sophisticated approach might attempt to prevent this "pollution" of the global namespace and prevent function naming collisions. 
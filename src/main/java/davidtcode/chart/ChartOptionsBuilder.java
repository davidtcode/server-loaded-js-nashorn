package davidtcode.chart;

import java.util.List;

import davidtcode.model.chart.ChartOptionsCodeBlock;
import davidtcode.model.readings.DeviceReading;

/**
 * @author David.Tegart
 */
public interface ChartOptionsBuilder {

	public String build(List<DeviceReading> readings, ChartOptionsCodeBlock codeBlock);
}
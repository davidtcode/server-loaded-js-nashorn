package davidtcode.chart;

/**
 * @author David.Tegart
 */
public class ChartOptionsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ChartOptionsException() {}

	public ChartOptionsException(String message) {
		super(message);
	}

	public ChartOptionsException(String message, Throwable t) {
		super(message, t);
	}
}
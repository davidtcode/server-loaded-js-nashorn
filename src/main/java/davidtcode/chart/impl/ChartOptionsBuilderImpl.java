package davidtcode.chart.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import davidtcode.chart.ChartOptionsBuilder;
import davidtcode.chart.ChartOptionsException;
import davidtcode.model.chart.ChartOptionsCodeBlock;
import davidtcode.model.readings.DeviceReading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author David.Tegart
 */
@Service("chartOptionsBuilder")
public class ChartOptionsBuilderImpl implements ChartOptionsBuilder {

	private static final Logger LOG = LoggerFactory
			.getLogger(ChartOptionsBuilderImpl.class.getCanonicalName());

	private ScriptEngineManager manager;
	private ScriptEngine scriptEngine;
	private Invocable invocable;
	
	private Map<String, String> cachedFunctions = new ConcurrentHashMap<>(10);
	
	@Value("${script.engine.name}")
	private String engineName;

	@Override
	public String build(List<DeviceReading> readings,
			ChartOptionsCodeBlock codeBlock) {

		Validate.notNull(readings);
		Validate.notNull(codeBlock);
		Validate.notBlank(codeBlock.getFunctionName());
		Validate.notBlank(codeBlock.getCode());

		try {

			String function = null;
			if((function = cachedFunctions.get(codeBlock.getFunctionName())) == null) {
				evaluateCode(codeBlock);
			} else {
				
				// Has the function changed?
				
				if(function.equals(codeBlock.getCode()) == false) { // code has changed
					evaluateCode(codeBlock);
				}
			}

			LOG.debug("Invoking |{}| for code-block {}", 
					codeBlock.getFunctionName(), codeBlock.getId());
			
			Object options = invokeOptionsBuilder(codeBlock.getFunctionName(), readings);
			
			// The chart options must be represented as a string
			if((options == null) || !(options instanceof String)) {
				throw new ChartOptionsException("Returned value for " + codeBlock.getId() + " isn't valid");
			}
			
			return (String) options;

		} catch (Exception ex) {

			LOG.error("The following code-block cannot be executed: |{}|", codeBlock.toString());
			
			throw new ChartOptionsException(
					"Code for " + codeBlock.getId() + " cannot be evaluated", ex);
		}
	}

	@PostConstruct
	public void init() {

		manager = new ScriptEngineManager();
		scriptEngine = manager.getEngineByName(engineName);
		
		Validate.notNull(scriptEngine);
		
		invocable = (Invocable) scriptEngine;
	}
	
	private Object invokeOptionsBuilder(String functionName, List<DeviceReading> readings) 
			throws NoSuchMethodException, ScriptException {
		return invocable.invokeFunction(functionName, readings);
	}

	private void evaluateCode(ChartOptionsCodeBlock codeBlock) throws Exception {

		boolean isError = false;
		try {
			
			scriptEngine.eval(codeBlock.getCode());

		} catch (Exception ex) {
			
			isError = true;
			
			throw ex;
		}
		finally {
			
			if(isError == false) {
				cachedFunctions.put(codeBlock.getFunctionName(), codeBlock.getCode());	
			}
		}
	}
}
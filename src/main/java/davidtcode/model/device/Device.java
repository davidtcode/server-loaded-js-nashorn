package davidtcode.model.device;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import davidtcode.model.readings.DeviceReading;

/**
 * @author David.Tegart
 */
@Entity
@Table(name = "device")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE)
public class Device {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty(value = "id")
	private long id;
	
	@JsonProperty(value = "deviceName")
	private String deviceName;
	
	@JsonProperty(value = "description")
	private String description;

	@JsonIgnore
	@OneToMany(mappedBy = "device", fetch = FetchType.EAGER)
	List<DeviceReading> readings;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DeviceReading> getReadings() {
		return readings;
	}

	public void setReadings(List<DeviceReading> readings) {
		this.readings = readings;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Device [id=");
		builder.append(id);
		builder.append(", deviceName=");
		builder.append(deviceName);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}
}
package davidtcode.model.readings;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import davidtcode.model.device.Device;

/**
 * @author David.Tegart
 */
@Entity
@Table(name = "device_readings")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE)
public class DeviceReading {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty(value = "id")
	private long id;

	@JsonProperty(value = "timestamp")
	private long timestamp;
	
	@JsonProperty(value = "value")
	private double value;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(nullable=true, name="device_id")
	private Device device;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}
}
package davidtcode.rasterizer;

import java.io.ByteArrayOutputStream;

/**
 * The job of a "rasterizer" is to convert a chart, as represented by the supplied
 * options, to a visual image. This could be a pdf, a jpeg and so on, the bytes 
 * of which are contained in the byte array stream.
 * 
 * @author David.Tegart
 */
public interface Rasterizer {

	public abstract ByteArrayOutputStream request(String requestOptions);
}
package davidtcode.rasterizer;

/**
 * @author David.Tegart
 */
public class RasterizerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RasterizerException() {}

	public RasterizerException(String message) {
		super(message);
	}

	public RasterizerException(String message, Throwable t) {
		super(message, t);
	}
}
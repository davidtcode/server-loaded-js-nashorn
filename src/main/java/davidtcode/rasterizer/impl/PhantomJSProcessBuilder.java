package davidtcode.rasterizer.impl;

import java.io.IOException;
import java.util.List;

/**
 * Defines the API for building the <code>Process</code> instance which is used
 * to start the PhantomJS process.
 * 
 * @author David.Tegart
 */
public interface PhantomJSProcessBuilder {

	public Process build(List<String> commands) throws IOException;
}
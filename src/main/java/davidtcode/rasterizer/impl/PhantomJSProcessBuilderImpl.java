package davidtcode.rasterizer.impl;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

/**
 * @author David.Tegart
 */
@Component("phantomJSProcessBuilder")
public class PhantomJSProcessBuilderImpl implements PhantomJSProcessBuilder {

	@Override
	public Process build(List<String> commands) throws IOException {

		Validate.notEmpty(commands);

		return new ProcessBuilder(commands).start();
	}
}
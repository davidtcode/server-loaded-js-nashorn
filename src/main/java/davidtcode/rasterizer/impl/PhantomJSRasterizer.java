package davidtcode.rasterizer.impl;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;

import davidtcode.rasterizer.Rasterizer;
import davidtcode.rasterizer.RasterizerException;

/**
 * Essentially a wrapper for a PhantomJS process which has been started in server
 * mode. 
 * 
 * <p/>
 * 
 * PhantomJS is responsible for generating the chart from the supplied data
 * and creating a "screen-grab" by saving it as a pdf.
 * 
 * @author David.Tegart
 */
@Service("rasterizer")
public class PhantomJSRasterizer implements Rasterizer {

	private static final Logger LOG = LoggerFactory
			.getLogger(PhantomJSRasterizer.class.getCanonicalName());

	public static final String KEY_REQ_PCK_TYPE = "constr";
	public static final String KEY_REQ_OUTFILE = "outfile";
	public static final String KEY_REQ_WIDTH = "width";
	public static final String KEY_REQ_CALLBACK = "callback";
	public static final String KEY_REQ_SCALE = "scale";
	public static final String KEY_REQ_INFILE = "infile";

	@Autowired
	private PhantomJSRasterizerFolder rasterizerFolder;
	
	@Autowired
	private PhantomJSProcessBuilder processBuilder;

	@Value("${phantomsjs.exec}")
	private String exec;
	
	@Value("${phantomsjs.port}")
	private int port;
	
	@Value("${phantomsjs.host}")
	private String host;

	@Value("${phantomsjs.resourcesPattern}")
	private String resourcesPattern;

	@Value("${phantomsjs.readTimeout}")
	private int readTimeout;
	
	@Value("${phantomsjs.connectTimeout}")
	private int connectTimeout;

	@Value("${phantomsjs.hc.scriptname}")
	private String scriptName;

	private String outgoingStreamEncoding = "utf-8";
	private String incomingStreamEncoding = "utf-8";

	private String outputFolder;
	private Process process;

	private String defaultChartPackage = "Chart";
	private String defaultOutfilePrefix = "";
	private String defaultOutfileExt = "pdf";
	private String defaultChartWidth = "1000.0";
	private String defaultChartScale = "1";

	JsonWriterFactory jsonWriterFactory = Json.createWriterFactory(null);

	public PhantomJSRasterizer() { }

	public ByteArrayOutputStream request(String chartOptions)
			throws RasterizerException {

		Validate.notNull(chartOptions);

		JsonObject payload = buildRequestPayload(chartOptions);

		LOG.trace("Making PhantomJS [port: {}] request with payload |{}|",
				port, payload);

		String response = null;
		ByteArrayOutputStream rasterizedImageAsStream;

		OutputStream out = null;
		InputStream in = null;
		JsonWriter jsonWriter = null;

		try {

			URL url = getServerUrl();

			URLConnection connection = getConnection(url);

			//
			// Write out to PhantomJS

			out = connection.getOutputStream();

			jsonWriter = jsonWriterFactory.createWriter(out,
					Charset.forName(outgoingStreamEncoding));
			jsonWriter.writeObject(payload);
			jsonWriter.close();

			//
			// Read in from PhantomJS

			in = connection.getInputStream();
			response = IOUtils.toString(in, incomingStreamEncoding);
			in.close();

			LOG.trace("The response is |{}|", response);

			//
			// Open the rasterised image file for the returned stream

			rasterizedImageAsStream = getRasterizedImageAsStream(response);

		} catch (Exception ex) {

			LOG.error("The PhantomsJS request has failed. Payload is |{}|",
					payload);

			throw new RasterizerException("PhantomJS request has failed", ex);

		} finally {

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					LOG.warn("Cannot close the output stream");
				}
			}

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					LOG.warn("Cannot close the input stream");
				}
			}

			if (jsonWriter != null) {
				try {
					jsonWriter.close();
				} catch (JsonException ex) {
					LOG.warn("There has been an error when closing the json-writer");
				}
			}
		}

		return rasterizedImageAsStream;
	}

	@PostConstruct
	public void init() {
		
		outputFolder = rasterizerFolder.getOutputFolder().toFile().getAbsolutePath();
		
		copyRasterizationResources();

		startPhantomJsServer();
	}
	
	@PreDestroy
	public void destroy() {

		LOG.debug("Destroying PhantomJS process running on port {}", port);

		try {

			process.getErrorStream().close();
			process.getInputStream().close();
			process.getOutputStream().close();

		} catch (IOException ex) {
			LOG.error("Error while shutting down process: {}", ex.getMessage());
		} finally {
			process.destroy();
			process = null;
		}
	}

	protected void startPhantomJsServer() {

		LOG.debug("Starting PhantomJS process with settings: "
				+ " host: |{}|, port: |{}|, exec: |{}|, script: |{}| "
				+ "outputFolder: |{}|", host, port, exec, scriptName, outputFolder);

		try {

			List<String> commands = Arrays.asList(exec, getScriptLocation(), "-host", host,
					"-port", Integer.toString(port), "-tmpdir", outputFolder);

			LOG.trace("The command list is |{}|", commands);

			process = processBuilder.build(commands);

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));

			String readLine = bufferedReader.readLine();

			if ((readLine == null) || !readLine.contains("ready")) {

				process.destroy();

				throw new RuntimeException(
						"PhantomJS process isn't telling me the right things: |"
								+ readLine + "|");
			}

		} catch (Exception ex) {
			throw new RuntimeException(
					"The PhantomsJS server cannot be started", ex);
		}

		//
		// Shutdown process on JVM exit

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (process != null) {

					LOG.info(
							"Shutting down PhantomJS instance with port ... {}",
							port);

					try {
						process.getErrorStream().close();
						process.getInputStream().close();
						process.getOutputStream().close();
					} catch (IOException ex) {
						LOG.warn(
								"Error while shutting down PhantomJS process: {}",
								ex.getMessage());
					}

					process.destroy();
				}
			}
		});

		LOG.info("PhantomJS server started on port {}", port);
	}

	private URL getServerUrl() throws MalformedURLException {

		StringBuilder url = new StringBuilder();
		url.append("http://");
		url.append(host);
		url.append(":");
		url.append(port);
		url.append("/");

		return new URL(url.toString());
	}

	private URLConnection getConnection(URL url) throws IOException {

		URLConnection connection = url.openConnection();
		connection.setDoOutput(true);
		connection.setConnectTimeout(connectTimeout);
		connection.setReadTimeout(readTimeout);

		return connection;
	}

	private JsonObject buildRequestPayload(String chartOptions) {

		/*
		 * Example request payload:
		 * 
		 * { "constr": "Chart", 
		 *   "outfile": "filename.pdf", 
		 *   "width": "1000.0", 
		 *   "scale": "1.0", 
		 *   "infile": * "{xAxis: {categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']},series: [{data: [2.9, 1.5, 1.4, 9.2, 4.0, 6.0, 5.6, 8.5, 6.4, 4.1, 5.6, 4.4]}]};"
		 * }
		 */

		JsonObjectBuilder requestPayload = Json.createObjectBuilder();

		requestPayload.add(KEY_REQ_PCK_TYPE, defaultChartPackage);
		requestPayload.add(KEY_REQ_OUTFILE, createRandomFileName());
		requestPayload.add(KEY_REQ_WIDTH, defaultChartWidth);
		requestPayload.add(KEY_REQ_SCALE, defaultChartScale);
		requestPayload.add(KEY_REQ_INFILE, chartOptions);

		return requestPayload.build();
	}

	private String createRandomFileName() {

		StringBuilder filename = new StringBuilder();

		if (defaultOutfilePrefix != null)
			filename.append(defaultOutfilePrefix);

		filename.append(RandomStringUtils.randomAlphanumeric(10));
		filename.append(".");
		filename.append(defaultOutfileExt);

		return filename.toString();
	}

	private ByteArrayOutputStream getRasterizedImageAsStream(String response)
			throws IOException {

		ByteArrayOutputStream rasterizedImageAsStream = new ByteArrayOutputStream();

		rasterizedImageAsStream.write(Files
				.readAllBytes(getPathToRasterizedImageFromResponse(response)));

		return rasterizedImageAsStream;
	}

	private Path getPathToRasterizedImageFromResponse(String response) {
		return Paths.get(outputFolder, response);
	}

	private void copyRasterizationResources() {

		URL url = getClass().getProtectionDomain().getCodeSource()
				.getLocation();

		URLClassLoader jarLoader = new URLClassLoader(new URL[] { url }, Thread
				.currentThread().getContextClassLoader());

		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(
				jarLoader);

		try {

			Resource[] resources = resolver.getResources(resourcesPattern);

			Path configFolder = rasterizerFolder.getConfigFolder();

			for (Resource resource : resources) {

				LOG.info("Copying {} to {}", resource.getFilename(),
						configFolder);

				Path path = Paths.get(configFolder.toString(),
						resource.getFilename());
				File newResourceFile = Files.createFile(path).toFile();
				newResourceFile.deleteOnExit();

				try (InputStream in = resource.getInputStream();
						OutputStream out = new FileOutputStream(newResourceFile)) {
					IOUtils.copy(in, out);
				}
			}

		} catch (IOException ex) {

			throw new RuntimeException(
					"Error while setting up phantomjs environment", ex);
		}
	}	

	private String getScriptLocation() {

		StringBuilder scriptLocation = new StringBuilder();
		scriptLocation.append(rasterizerFolder.getConfigFolder().toAbsolutePath().toString());
		scriptLocation.append(FileSystems.getDefault().getSeparator());
		scriptLocation.append(scriptName);

		return scriptLocation.toString();
	}
}
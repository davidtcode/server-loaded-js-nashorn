package davidtcode.rasterizer.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * A class used to create the folders required by PhantomJS.
 * 
 * @author David.Tegart
 */
@Component("rasterizerFolder")
public class PhantomJSRasterizerFolder {

	private static final Logger LOG = LoggerFactory
			.getLogger(PhantomJSRasterizerFolder.class.getCanonicalName());

	private Path rootFolder;
	private Path outputFolder;
	private Path configFolder;

	public static final String TMP_FOLDER_PATH = System.getProperty("java.io.tmpdir");

	public static final String RASTERIZER_FOLDER_NAME = "rasterizer";
	public static final String OUTPUT_FOLDER_NAME = "output";
	public static final String CONFIG_FOLDER_NAME = "config";

	public PhantomJSRasterizerFolder() {}

	public Path getRootFolder() {
		return rootFolder;
	}

	public Path getOutputFolder() {
		return outputFolder;
	}

	public Path getConfigFolder() {
		return configFolder;
	}

	@PostConstruct
	protected void init() throws IOException {

		Validate.notBlank(TMP_FOLDER_PATH);

		FileUtils.deleteQuietly(Paths.get(TMP_FOLDER_PATH, RASTERIZER_FOLDER_NAME).toFile());

		rootFolder = Files.createDirectory(Paths.get(TMP_FOLDER_PATH, RASTERIZER_FOLDER_NAME));
		rootFolder.toFile().deleteOnExit(); // Delete on JVM exit

		outputFolder = Files.createDirectory(Paths.get(rootFolder.toString(), OUTPUT_FOLDER_NAME));
		outputFolder.toFile().deleteOnExit();

		configFolder = Files.createDirectory(Paths.get(rootFolder.toString(), CONFIG_FOLDER_NAME));
		configFolder.toFile().deleteOnExit();

		LOG.info("Initialising rasterizer folder: |{}|", rootFolder);

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {

				LOG.info("Deleting {}", rootFolder);
				
				FileUtils.deleteQuietly(rootFolder.toFile());
			}
		});
	}
}
package davidtcode.repository.chart;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import davidtcode.model.chart.ChartOptionsCodeBlock;

/**
 * @author David.Tegart
 */
public interface ChartOptionsCodeRepository extends CrudRepository<ChartOptionsCodeBlock, Long> {
	
	public List<ChartOptionsCodeBlock> findByName(String name);
	
	public List<ChartOptionsCodeBlock> findAll();
}
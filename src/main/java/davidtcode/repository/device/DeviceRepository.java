package davidtcode.repository.device;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import davidtcode.model.device.Device;

/**
 * @author David.Tegart
 */
public interface DeviceRepository extends CrudRepository<Device, Long> {
	
	public Device findById(long id);
	
	public List<Device> findAll();
}
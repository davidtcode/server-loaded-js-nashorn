package davidtcode.repository.readings;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import davidtcode.model.readings.DeviceReading;

/**
 * @author David.Tegart
 */
public interface DeviceReadingsRepository extends CrudRepository<DeviceReading, Long> {
	
	public List<DeviceReading> findByDeviceId(long deviceId);
}
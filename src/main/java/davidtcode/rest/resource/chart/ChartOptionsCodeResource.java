package davidtcode.rest.resource.chart;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import davidtcode.model.chart.ChartOptionsCodeBlock;
import davidtcode.repository.chart.ChartOptionsCodeRepository;

/**
 * @author David.Tegart
 */
@Path("/chart")
public class ChartOptionsCodeResource {

	private static final Logger LOG = LoggerFactory
			.getLogger(ChartOptionsCodeResource.class.getCanonicalName());

	@Autowired
	private ChartOptionsCodeRepository chartOptionsCodeRepository;

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<ChartOptionsCodeBlock> getAllCharts() {
		
		LOG.debug("Getting all the charts");

		return chartOptionsCodeRepository.findAll(); 
	}
	
	@GET
	@Path("/option/code")
	@Produces("text/javascript")
	@Consumes({ "text/javascript", "application/javascript" })
	public String getAllChartOptionsCode() {
		
		LOG.debug("Getting all the chart options code");

		List<ChartOptionsCodeBlock> codeBlocks = chartOptionsCodeRepository.findAll(); 
		
		StringBuilder codeBlock = new StringBuilder();
		for(ChartOptionsCodeBlock chartOptionsCodeBlock : codeBlocks) {
			codeBlock.append(chartOptionsCodeBlock.getCode());
		}

		return codeBlock.toString();
	}
}
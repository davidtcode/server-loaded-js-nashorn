package davidtcode.rest.resource.readings;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import davidtcode.model.device.Device;
import davidtcode.model.readings.DeviceReading;
import davidtcode.repository.device.DeviceRepository;
import davidtcode.repository.readings.DeviceReadingsRepository;

/**
 * @author David.Tegart
 */
@Path("/device")
public class DeviceResource {

	private static final Logger LOG = LoggerFactory
			.getLogger(DeviceResource.class.getCanonicalName());

	@Autowired
	private DeviceReadingsRepository deviceReadingsRepository;

	@Autowired
	private DeviceRepository deviceRepository;

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Device> getAllDevices() {
		
		LOG.debug("Getting all devices");

		return deviceRepository.findAll();
	}
	
	@GET
	@Path("/{deviceId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Device getDevice(@PathParam("deviceId") long deviceId) {
		
		LOG.debug("Getting device {}", deviceId);

		return deviceRepository.findById(deviceId);
	}

	@GET
	@Path("/readings/{deviceId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<DeviceReading> getDeviceReading(
			@PathParam("deviceId") long deviceId) {
		
		LOG.debug("Getting readings for device {}", deviceId);

		return deviceReadingsRepository.findByDeviceId(deviceId);
	}
}
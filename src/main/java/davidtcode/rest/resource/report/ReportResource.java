package davidtcode.rest.resource.report;

import java.io.ByteArrayOutputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import davidtcode.service.report.DeviceReportService;

/**
 * @author David.Tegart
 */
@Path("/report")
public class ReportResource {

	private static final Logger LOG = LoggerFactory
			.getLogger(ReportResource.class.getCanonicalName());

	@Autowired
	private DeviceReportService deviceReportService;

	@GET
	@Path("{deviceId}")
	@Produces("application/pdf")
	public Response getReportForDevice(
			@PathParam("deviceId") long deviceId,
			@QueryParam("chart") long chartCodeId) {
		
		LOG.debug("Getting report for device {} using chart {}", 
				deviceId, chartCodeId);

		ByteArrayOutputStream output = deviceReportService.getReport(deviceId, chartCodeId);

		return Response.ok(output.toByteArray())
				.header("content-disposition", "attachment; filename=report.pdf")
				.build(); 
	}
}
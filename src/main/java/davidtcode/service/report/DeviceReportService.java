package davidtcode.service.report;

import java.io.ByteArrayOutputStream;

import org.springframework.transaction.annotation.Transactional;

/**
 * @author David.Tegart
 */
public interface DeviceReportService {

	@Transactional(readOnly = true)
	public ByteArrayOutputStream getReport(long deviceId, long chartCodeId);
}
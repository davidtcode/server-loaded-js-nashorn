package davidtcode.service.report.impl;

import java.io.ByteArrayOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import davidtcode.chart.ChartOptionsBuilder;
import davidtcode.rasterizer.Rasterizer;
import davidtcode.repository.chart.ChartOptionsCodeRepository;
import davidtcode.repository.readings.DeviceReadingsRepository;
import davidtcode.service.report.DeviceReportService;

/**
 * @author David.Tegart
 */
@Service("deviceReportService")
public class DeviceReportServiceImpl implements DeviceReportService {

	private static final Logger LOG = LoggerFactory
			.getLogger(DeviceReportServiceImpl.class);

	@Autowired
	private Rasterizer rasterizer;
	
	@Autowired
	private DeviceReadingsRepository deviceReadingsRepository;

	@Autowired
	private ChartOptionsCodeRepository chartOptionsCodeRepository;
	
	@Autowired
	public ChartOptionsBuilder chartOptionsBuilder;
	
	@Override
	public ByteArrayOutputStream getReport(long deviceId, long chartCodeId) {

		String options = chartOptionsBuilder.build(
				deviceReadingsRepository.findByDeviceId(deviceId), 
				chartOptionsCodeRepository.findOne(chartCodeId));

		LOG.debug("The options for device {} and chart {} are |{}|",
				deviceId, chartCodeId, options);

		return rasterizer.request(options);
	}
}

-- Inserts for the device table

INSERT INTO device(id,deviceName,description) VALUES(1,'Downstairs Lighting','The device controlling the lights downstairs');
INSERT INTO device(id,deviceName,description) VALUES(2,'Upstairs HVAC','The upstairs device controlling HVAC');
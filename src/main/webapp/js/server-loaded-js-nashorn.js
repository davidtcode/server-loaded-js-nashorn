/**
 * A generic object used to plot chart and request the report version of a chart.
 */

function ChartPlotter(chartCodeUri, deviceListUri, chartListUri, readingsUri, reportUri) {
	
	this.chartCodeUri = chartCodeUri;
	this.deviceListUri = deviceListUri;
	this.chartListUri = chartListUri;
	this.readingsUri = readingsUri;
	this.reportUri = reportUri;
	
	this.chartIdFunctionNameMap = {};

	this.drawChartCallback = _.after(2, this.drawChart);

	this.loadChartCode();
}

ChartPlotter.prototype.addAlert = function(message, type) {

	$('#alerts').html(
		'<div class="alert alert-' + type
				+ '" style="display:none;" id="alertMessage" role="alert">'
				+ '<button type="button" class="close" data-dismiss="alert">'
				+ '&times;</button><strong>' + message
				+ '</strong></div>');

	$('#alertMessage').slideDown(400);

	window.setTimeout(function() {
		$("#alertMessage").slideUp(400, function() {
			$(this).remove();
		});
	}, 2000);
}
	
ChartPlotter.prototype.loadChartCode = function() {

	var _self = this;

	$.getScript(this.chartCodeUri).done(function() {
		_self.populateChartList();
		_self.populateDeviceList();
	}).fail(function() {
		_self.addAlert('Cannot load the chart options builder code', 'danger');
	});
}

ChartPlotter.prototype.populateDeviceList = function() {

	var _self = this;
	var html = [];

	$.ajax({
		type : 'GET',
		url : this.deviceListUri,
		success: function(response) {

			html.push('<select class="form-control input-sm" id="deviceList">');
			$.each(response, function(index, device) {
				html.push('<option value="' + device.id + '">' + device.deviceName + '</option>');
			});
			html.push('</div>');
			
			$('#deviceListContainer').html(html.join(''));
			
			_self.drawChartCallback();
		},
		error: function(response) {
			_self.addAlert("There has been an error getting the list of devices", 'danger');
		}
	});	
}

ChartPlotter.prototype.populateChartList = function() {

	var _self = this;
	var html = [];
	
	$.ajax({
		type : 'GET',
		url : this.chartListUri,
		success : function(response) {

			html.push('<select class="form-control input-sm" id="chartList">');
			$.each(response, function(index, chart) {
				html.push('<option value="' + chart.id + '">' + chart.name + '</option>');
				
				_self.chartIdFunctionNameMap[chart.id] = chart.functionName;
			});
			html.push('</div>');
			
			$('#chartListContainer').html(html.join(''));
			
			_self.drawChartCallback();
		},
		error : function(response) {
			_self.addAlert("There has been an error getting the list of charts", 'danger');
		}
	});
}

ChartPlotter.prototype.drawChart = function() {

	var deviceId = $("#deviceList").val();
	var chartId = $("#chartList").val();
	var functionName = this.chartIdFunctionNameMap[chartId];
	var _self = this;
	var chartOptions = {};

	$.ajax({
		type : 'GET',
		url : this.readingsUri + '/' + deviceId,
		success : function(response) {

			try {
				chartOptions = window[functionName](response);
			} catch(err) {
				addAlert("There has been an error calling the plotting function", 'danger');
			}
			
			$('#chartArea').highcharts(JSON.parse(chartOptions));
		},
		error : function(response) {
			_self.addAlert("There has been an error plotting the chart", 'danger');
		}
	});
}

ChartPlotter.prototype.getReport = function() {

	var deviceId = $("#deviceList").val();
	var chartId = $("#chartList").val();

	window.open(this.reportUri + '/' + deviceId + '?chart=' + chartId, '_blank');
}